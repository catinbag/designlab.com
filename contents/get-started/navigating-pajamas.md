---
name: Navigating Pajamas
---

Our design system is broken down into the following sections:

## Foundations

Foundational items communicate the opinionated way in which basic visual design attributes, elements, and concepts come together to create a distinct GitLab personality that’s expressed in the UI. For example, color, typography, and iconography.

## Components

A component is a UI element that serves a singular purpose or function. Two or more simple components can combine to form a composite component that still has a single function. For example, an avatar, button, or combobox.

## Patterns

A pattern combines components and/or content elements into a repeatable, consistent group that has a shared purpose, content association, or both. For example, filtering, forms, or navigation.

## Page templates

A template prescribes layout and behavior for a page or common content layout. For example, an issuable, settings, or search results.

## Objects

An [object](/objects/overview) is a conceptual building block or concept that defines how we think about something independent of its visual representation or interaction model. For example, a [job](/objects/job), [merge request](/objects/merge-request), or repository.

## Content

Content includes documentation relating to our writing style. This includes the tone and voice of the brand, as well as common grammar guidelines.

## Usability

Usability guidelines include documentation that affects the ease-of-use for different types of users. This includes accessibility and internationalization.

## Resources

Our resources section contains relevant and useful links that aide in the creation of our design system, as well as GitLab design as a whole.
